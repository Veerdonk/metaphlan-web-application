package Metaphlan;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Defines the cli arguments as used by metaphlan
 * @author dvandeveerdonk
 */
public class CliArgument {
    private final String metaPhlan;
    private final String[] flanArgs;
    private final String inFile;
    private final int cores;
    private final String inputType;
    private final String outputName;
    private final String taxLev;
    private final String sens;
    private final int minCu;
    private final boolean ignoreE;
    private final boolean ignoreV;
    private final boolean ignoreA;
    private final boolean ignoreB;

    /**
     * Constructor
     * @param metaPhlan
     * @param inFile
     * @param cores
     * @param inputType
     * @param outputName
     * @param sens
     * @param taxLev
     * @param minCu
     * @param ignoreE
     * @param ignoreV
     * @param ignoreA
     * @param ignoreB
     */
    public CliArgument(String metaPhlan, String inFile, int cores, String inputType, String outputName, String taxLev, String sens, int minCu, boolean ignoreE, boolean ignoreV, boolean ignoreA, boolean ignoreB) {
        this.metaPhlan = metaPhlan;
        this.inFile = inFile;
        this.cores = cores;
        this.inputType = inputType;
        this.outputName = outputName;
        this.taxLev = taxLev;
        this.sens = sens;
        this.minCu = minCu;
        this.ignoreE = ignoreE;
        this.ignoreV = ignoreV;
        this.ignoreA = ignoreA;
        this.ignoreB = ignoreB;
        this.flanArgs = this.buildCommand();
    }
    /**
    * Returns the String array containing cli commands
    * @return
    */
    public String[] getFlanArgs() {
        return flanArgs;
    }

    /**
    * Creates the cli command based on properties
    * @return
    */
    private String[] buildCommand(){
        ArrayList<String> commands =  new ArrayList<>();
        commands.addAll(Arrays.asList(this.metaPhlan, this.inFile, "--input_type", this.inputType, "--nproc", Integer.toString(this.cores), "-o", this.outputName, "--tax_lev", this.taxLev, "--bt2_ps", this.sens,  "--min_cu_len", Integer.toString(this.minCu)));
        if(this.ignoreA == true){
            commands.add("--ignore_archea");
        }
        if(this.ignoreB == true){
            commands.add("--ignore_bacteria");
        }
        if(this.ignoreE ==  true){
            commands.add("--ignore_euakryotes");
        }
        if(this.ignoreV == true){
            commands.add("--ignore_viruses");
        }
        String[] commandArray = commands.toArray(new String[commands.size()]);
        return commandArray;
    }

}
