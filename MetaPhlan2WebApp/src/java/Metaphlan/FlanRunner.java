package Metaphlan;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Runs Metaphlan
 * @author dvandeveerdonk
 */
public class FlanRunner implements FlanData {
    /**
     * Uses a CliArgument to execute metaphlan with its settings.
     * @param flanArgs
     * @throws ArgumentError
     */
    @Override
    public void executeFlan(CliArgument flanArgs) throws ArgumentError {
        try {
            String[] command = flanArgs.getFlanArgs();
            System.out.println(Arrays.toString(flanArgs.getFlanArgs()));
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            Logger.getLogger(FlanRunner.class.getName()).log(Level.INFO, "MetaPhlAn2.py started");
            try {
                p.waitFor();}
            catch (InterruptedException ex) {
                Logger.getLogger(FlanRunner.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(FlanRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
