package Metaphlan;

/**
 * Defines the argumenterror
 * @author dvandeveerdonk
 */
public class ArgumentError extends Exception {

    /**
     *
     */
    public ArgumentError() {
    }

    /**
     *
     * @param message
     */
    public ArgumentError(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public ArgumentError(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param cause
     */
    public ArgumentError(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ArgumentError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
