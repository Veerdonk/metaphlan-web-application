package Metaphlan;

/**
 * Used for creating a CliArgument with default values
 * @author dvandeveerdonk
 */
public class ArgumentBuilder {
    private String metaPhlan = "";
    private String inFile = "";
    private int cores = 1;
    private String inType = "fasta";
    private String outName = "default";
    private String taxLev = "a";
    private String sens = "very-sensitive";
    private int minCu = 2000;
    private boolean ignoreE = false;
    private boolean ignoreV = false;
    private boolean ignoreA = false;
    private boolean ignoreB = false;

    /**
     * sets the metaphlan path
     * @param metaPhlan
     * @return this object
     */
    public ArgumentBuilder setMetaphlan(String metaPhlan){
     this.metaPhlan = metaPhlan;
     return this;
    }

    /**
     * sets the inputfile
     * @param inFile
     * @return this object
     */
    public ArgumentBuilder setInFile(String inFile){
     this.inFile = inFile;
     return this;
    }

    /**
     * Sets the number of cores to use
     * @param cores
     * @return this object
     */
    public ArgumentBuilder setCores(int cores){
        int sysCores = Runtime.getRuntime().availableProcessors();
        if(cores > sysCores){ //if the given number of cores exceeds that of the 
            this.cores = sysCores;//system set cores to number of cores present
        }
        else{
            this.cores = cores;
        }
        return this;
    }

    /**
     * Sets the type of the inputfile
     * @param inType
     * @return this object
     */
    public ArgumentBuilder setInType(String inType){
        this.inType = inType;
        return this;
    }

    /**
     * Sets the desired output name
     * @param outName
     * @return this object
     */
    public ArgumentBuilder setOutName(String outName){
        this.outName = outName+".txt";
        return this;
    }

    /**
     * Sets the taxonomic level to analyze
     * @param taxLev
     * @return this object
     */
    public ArgumentBuilder setTaxLev(String taxLev){
     this.taxLev = taxLev;
     return this;
    }

    /**
     * Sets the sensitivity
     * @param sens
     * @return this object
     */
    public ArgumentBuilder setSens(String sens){
      this.sens = sens;
      return this;
    };

    /**
     * Sets the minimal nucleotide length
     * @param minCu
     * @return this object
     */
    public ArgumentBuilder setMinCu(int minCu){
      this.minCu = minCu;
      return this;
    };

    /**
     * Set whether eukaryotes should be ignored
     * @param ignore
     * @return this object
     */
    public ArgumentBuilder setIgnoreE(boolean ignore){
        this.ignoreE = ignore;
        return this;
    }

    /**
     * Set whether Archea should be ignored
     * @param ignore
     * @return this object
     */
    public ArgumentBuilder setIgnoreA(boolean ignore){
        this.ignoreA = ignore;
        return this;
    }

    /**
     * Set whether virusses should be ignored
     * @param ignore
     * @return this object
     */
    public ArgumentBuilder setIgnoreV(boolean ignore){
        this.ignoreV = ignore;
        return this;
    }

    /**
     * Set whether bacteria should be ignored
     * @param ignore
     * @return this object
     */
    public ArgumentBuilder setIgnoreB(boolean ignore){
        this.ignoreB = ignore;
        return this;
    }

    /**
     * Creates a new CliArgument using all of the settings
     * @return this object
     */
    public CliArgument build(){
        return new CliArgument(metaPhlan, inFile, cores, inType, outName, taxLev, sens, minCu, ignoreE, ignoreV, ignoreA, ignoreB);
    }
}
