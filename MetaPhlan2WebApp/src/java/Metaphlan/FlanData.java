package Metaphlan;

/**
 * Interface for executing metaphlan
 * @author dvandeveerdonk
 */
interface FlanData {
    /**
     * @param args
     * @throws ArgumentError
     */
    void executeFlan(CliArgument args) throws ArgumentError;


}
