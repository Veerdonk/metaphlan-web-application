package dao;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Connects to a MySQL database
 * @author dvandeveerdonk
 */
public class Connector implements DBInterface{
    private Connection connection;
    private PreparedStatement getUserPass;
    private PreparedStatement addUser;
    private PreparedStatement checkUser;
    private String DbUrl;
    private String user;
    private String DbPass;

    /**
     * Retrieves connection information from a properties file
     */
    public Connector() {
        try {
            Properties prop = new Properties();
            prop.load(getClass().getResourceAsStream("DBProps.properties"));
            this.DbUrl = prop.getProperty("url");
            this.user = prop.getProperty("user");
            this.DbPass = prop.getProperty("pass");
        } catch (IOException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Connects to the database using data from properties file
     * @throws ConnectionError 
     */
    @Override
    public void connect() throws ConnectionError {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(this.DbUrl, this.user, this.DbPass);
            Logger.getLogger(Connector.class.getName()).log(Level.INFO, "Connection Established");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Prepares queries for use on the database
     * @throws SQLException
     */
    public void prepare() throws SQLException{
        String getUserQuery = "SELECT * FROM users WHERE user_name=? AND user_password=?";
        String addUserQuery = "INSERT INTO users (user_name, user_password, user_email) VALUES(?,?,?)";
        String checkUserQuery = "SELECT * FROM users WHERE user_name=?";
        getUserPass = connection.prepareStatement(getUserQuery);
        addUser = connection.prepareStatement(addUserQuery);
        checkUser = connection.prepareStatement(checkUserQuery);
    }

    /**
     * Ends the connection with the database
     */
    @Override
    public void disconnect() {
        try {
            this.getUserPass.close();
            this.connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a user if it exists in the database
     * @param userName
     * @param password
     * @return
     * @throws ConnectionError
     */
    @Override
    public User getUser(String userName, String password) throws ConnectionError {
        try {
            this.getUserPass.setString(1, userName);
            this.getUserPass.setString(2, password);
            ResultSet results = getUserPass.executeQuery();
            if (results.next()){
                String email = results.getString("user_email");
                return new User(userName, email);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * checks whether a username is present in the database
     * @param userName
     * @return
     */
    public User checkUser(String userName){
        try {
            this.checkUser.setString(1, userName);
            ResultSet results = checkUser.executeQuery();
            if(results.next()){
                String email = results.getString("user_email");
                return new User(userName, email);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * inserts a new user into the database
     * @param userName
     * @param password
     * @param email
     * @throws ConnectionError
     */
    @Override
    public void insertUser(String userName, String password, String email) throws ConnectionError {
        try {
            this.addUser.setString(1, userName);
            this.addUser.setString(2, password);
            this.addUser.setString(3, email);
            addUser.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
