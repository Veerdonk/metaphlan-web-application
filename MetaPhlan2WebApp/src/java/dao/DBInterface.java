/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author dvandeveerdonk
 */
public interface DBInterface {
    /**
     * Establishes a connection to datasource.
     * @throws ConnectionError ex
     */
    void connect() throws ConnectionError;

    /**
     *
     */
    void disconnect();

    /**
     *
     * @param userName
     * @param passWord
     * @return
     * @throws ConnectionError
     */
    User getUser(String userName, String passWord) throws ConnectionError;

    /**
     *
     * @param userName
     * @param passWord
     * @param email
     * @throws ConnectionError
     */
    void insertUser(String userName, String passWord, String email) throws ConnectionError;
}

