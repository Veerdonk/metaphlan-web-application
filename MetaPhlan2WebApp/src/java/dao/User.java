/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 * Defines the user object
 * @author dvandeveerdonk
 */
public class User {
    private String userName;
    private String email;

    /**
     * sets the username
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * sets users email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * returns username
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * returns email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * constructs the user object
     * @param userName
     * @param email
     */
    public User(String userName, String email) {
        this.userName = userName;
        this.email = email;
    }
}
