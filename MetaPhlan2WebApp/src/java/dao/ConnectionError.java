package dao;

/**
 * Defines the error thrown when connection fails
 * @author dvandeveerdonk
 */
public class ConnectionError extends Exception{

    /**
     *
     */
    public ConnectionError() {
    }

    /**
     *
     * @param message The error message
     */
    public ConnectionError(String message) {
        super(message);
    }

    /**
     *
     * @param message the error message
     * @param cause the error cause
     */
    public ConnectionError(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param cause
     */
    public ConnectionError(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ConnectionError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
