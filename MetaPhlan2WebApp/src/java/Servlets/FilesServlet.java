package Servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles the files uploaded by the user.
 * @author dvandeveerdonk
 */
@WebServlet(name = "FilesServlet", urlPatterns = {"/viewFiles.do"})
public class FilesServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * Forwards GET method to go back to the home page.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * Stores the uploaded files from the user in the userData map
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String dir = getServletContext().getRealPath("/") + "userData/" + session.getAttribute("userName") + "/";
        String page = "home.jsp";
        if (request.getParameter("files") != null) {
            String fileToDl = request.getParameter("files");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileToDl);
            File file = new File(dir + fileToDl);
            ServletOutputStream out;
            try (FileInputStream fileIn = new FileInputStream(file)) {
                out = response.getOutputStream();
                byte[] outputByte = new byte[4096];
                //copy binary contect to output stream
                while (fileIn.read(outputByte, 0, 4096) != -1) {
                    out.write(outputByte, 0, 4096);
                }
            }
            out.flush();
            out.close();
        }
        if (request.getParameter("krona") != null) {
            page = "userData/" + session.getAttribute("userName") + "/" + request.getParameter("krona");
        }

        RequestDispatcher view = request.getRequestDispatcher(page);
        view.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
