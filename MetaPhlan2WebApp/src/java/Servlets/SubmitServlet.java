package Servlets;

import Krona.Controller;
import Metaphlan.ArgumentBuilder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.tomcat.util.http.fileupload.IOUtils;

/**
 * Retrieves user input and forwards it to be used with krona/metaphlan.
 * @author ahclugtenberg
 */
@WebServlet(name = "SubmitServlet", urlPatterns = {"/submit.do"})
@MultipartConfig
public class SubmitServlet extends HttpServlet {
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * Forwards GET method to go back to the home page.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * Gets the user input from the server and forwards this to be used via metaphlan/krona.
     * After metaphlan/krona is done, session will be invalidated and user will be send back
     * to login screen. 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String absFileSys = getServletContext().getRealPath("/");
        List<Part> fileParts = request.getParts().stream().filter(part -> "file".equals(part.getName())).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">
        HttpSession session = request.getSession();
        String metaPath = getServletContext().getInitParameter("metaphlanPath");
        String kronaPath = getServletContext().getInitParameter("kronaPath");
        String converterPath = getServletContext().getInitParameter("converterPath");
        Controller ctrl = new Controller();
        
//        Parameters are set to be used with the ArgumentBuilder
        ArgumentBuilder ab = new ArgumentBuilder();
        String inputFileType = request.getParameter("inputFileType");
        ab.setInType(inputFileType);
        ab.setMetaphlan(getServletContext().getInitParameter("metaphlanPath"));

        if (inputFileType.equals("multifasta")) {
            ab.setSens(request.getParameter("sensitivity"));
        }

        String taxLevel = request.getParameter("taxonomicLevel");
        ab.setTaxLev(taxLevel);

        if (request.getParameter("minCuLen").length() != 0) {
            int minCuLen = Integer.parseInt(request.getParameter("minCuLen"));
            ab.setMinCu(minCuLen);
        }

        if (request.getParameter("cores").length() != 0) {
            int cores = Integer.parseInt(request.getParameter("cores"));
            ab.setCores(cores);
        }

        if (request.getParameterValues("ignoreOrganisms") != null) {
            String[] organism = request.getParameterValues("ignoreOrganisms");
            for (String s : organism) {
                if (s.contains("Viruses")) {
                    ab.setIgnoreV(true);
                }
                if (s.contains("Eukaryotes")) {
                    ab.setIgnoreE(true);
                }
                if (s.contains("Bacteria")) {
                    ab.setIgnoreB(true);
                }
                if (s.contains("Archaea")) {
                    ab.setIgnoreA(true);
                }
            }
        }
        
//        Uploaded files are stored in a folder with the name of the user in the userData map
        String curUser = (String) session.getAttribute("userName");
        ArrayList mo = new ArrayList();
        File userdir = new File(absFileSys + "userData/" + curUser);
        userdir.mkdirs();
        String fileName = "";
        for (Part filePart : fileParts) {
            fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
            InputStream fc = filePart.getInputStream();
            File target = new File(absFileSys + "userData/" + curUser + "/" + fileName);
            OutputStream os = new FileOutputStream(target);

            java.nio.file.Files.copy(
                    fc,
                    target.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            IOUtils.closeQuietly(fc);

            ab.setInFile(target.getAbsolutePath());
            String outpath = target.getAbsolutePath() + "_METAPHLAN_OUT";
            ab.setOutName(outpath);
            ctrl.runMetaphlan(ab);
            mo.add(outpath + ".txt");
        }
        
//        When Krona is finished, session will be invalidated and user will be send back to the home page
        String kronaOut = ctrl.generateKrona(kronaPath, converterPath, userdir.getAbsolutePath() + "/" + fileName, mo);
        session.invalidate();
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
