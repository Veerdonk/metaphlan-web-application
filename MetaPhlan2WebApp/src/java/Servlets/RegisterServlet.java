package Servlets;

import dao.ConnectionError;
import dao.Connector;
import dao.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Will store information about the user in the database, if a new user registers.
 *
 * @author dvandeveerdonk
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register.do"})
public class RegisterServlet extends HttpServlet {

    Connector conn;

    /**
     * Initializes a connection to the database.
     */
    @Override
    public void init() {
        try {
            this.conn = new Connector();
            conn.connect();
            conn.prepare();
        } catch (ConnectionError | SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * Forwards GET method to go back to the home page.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * Stores information about the newly registerd user into the database.
     * Sets attribute errorMessage if something went wrong while creating the account, this message is then
     * shown to the user
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String page = "home.jsp";

        if (userName.length() < 6) {
            request.setAttribute("errorMessage", "Username should be more than 6 characters");
        } else {
            User u = conn.checkUser(userName);
            if (u == null) {
                try {
                    conn.insertUser(userName, password, email);
                    page = "home.jsp";
                    session.setAttribute("userName", userName);
                    session.setAttribute("email", email);
                } catch (ConnectionError ex) {
                    Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
                    request.setAttribute("errorMessage", "An error occurred, please try again later");
                }
            } else {
                request.setAttribute("errorMessage", "User already exists");
            }
        }
        RequestDispatcher view = request.getRequestDispatcher(page);
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
