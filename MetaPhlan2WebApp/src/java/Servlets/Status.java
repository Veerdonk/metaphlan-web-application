/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

/**
 * Is used to check the login status.
 * @author dvandeveerdonk
 */
public enum Status {

    /**
     *
     */
    LOGGED_IN,

    /**
     *
     */
    USER_UNKNOWN,

    /**
     *
     */
    ERROR;
}
