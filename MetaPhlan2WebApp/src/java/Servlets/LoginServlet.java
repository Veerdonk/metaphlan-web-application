package Servlets;

import dao.ConnectionError;
import dao.Connector;
import dao.User;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dvandeveerdonk
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login.do"})
public class LoginServlet extends HttpServlet {

    Connector conn;
    String email;

    /**
     * Initializes the connection to the database.
     */
    @Override
    public void init() {
        try {
            this.conn = new Connector();
            conn.connect();
            conn.prepare();
        } catch (ConnectionError | SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * Forwards GET method to go back to the home page.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * Checks if user exists in the database and can be logged in. If user exists and already has run a file to be used with krona
     * these files will be shown under the download/view section of the page.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = request.getParameter("userName");
        String userPass = request.getParameter("password");
        if (session.isNew() || session.getAttribute("userName") == null) {
            Status status = login(userName, userPass);
            switch (status) {
                case LOGGED_IN: {
                    session.setAttribute("userName", userName);
                    File dir = new File(getServletContext().getRealPath("/") + "userData/" + userName);
                    Logger.getLogger(LoginServlet.class.getName()).log(Level.INFO, "{0}userData/", getServletContext().getRealPath("/"));
                    if (dir.exists()) {
                        ArrayList files = new ArrayList();
                        ArrayList kronaFiles = new ArrayList();
                        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
                                Paths.get(dir.toString()), "*.*")) {
                            dirStream.forEach(path -> {
                                if (path.getFileName().toString().contains("KRONA")) {
                                    kronaFiles.add(path.getFileName().toString());
                                }
                                files.add(path.getFileName().toString());
                            });
                        }
                        session.setAttribute("data", files);
                        session.setAttribute("krona", kronaFiles);
                    }
                    session.setAttribute("email", this.email);
                    break;
                }
                case USER_UNKNOWN: {
                    request.setAttribute("errorMessage", "Your login credentials are incorrect, "
                            + "please try again or register.");
                    break;
                }
                case ERROR: {
                    request.setAttribute("errorMessage", "An error occurred, please try again later.");
                    break;
                }
            }
        }
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Checks if the user is known, and can be logged in.
     * @param userName username of the user
     * @param password password of the user
     * @return returns user unknown, logged in or error
     */
    private Status login(String userName, String password) {
        try {
            User user = conn.getUser(userName, password);
            if (user == null) {
                return Status.USER_UNKNOWN;
            } else {
                this.email = user.getEmail();
                return Status.LOGGED_IN;
            }
        } catch (ConnectionError ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            return Status.ERROR;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    } // </editor-fold>

}
