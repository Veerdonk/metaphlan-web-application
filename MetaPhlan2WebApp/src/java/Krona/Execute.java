package Krona;

import java.io.IOException;

/**
 * Interface for classes that execute external programs
 * @author dvandeveerdonk
 */
public interface Execute {

    /**
     * executes the executable
     * @param executable
     * @param userID
     * @param dataLocation
     * @throws java.io.IOException
     */
    public void execute(String executable, String userID, String dataLocation) throws IOException;

}
