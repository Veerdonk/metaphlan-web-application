package Krona;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Converts the output of metaphlan to a krona compatible .xml file
 * @author dvandeveerdonk
 */
public class MetaphlanConverter implements Execute{
    /**
     * Runs the converter script
     * @param converterLoc
     * @param inFiles
     * @param outFile
     */
    @Override
    public void execute(String converterLoc, String inFiles, String outFile){
        try {
            ProcessBuilder builder = new ProcessBuilder(converterLoc, "-pd", inFiles, "-o", outFile);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            Logger.getLogger(MetaphlanConverter.class.getName()).log(Level.INFO, "KronaConverter started");
            try {
                p.waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(MetaphlanConverter.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(MetaphlanConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
