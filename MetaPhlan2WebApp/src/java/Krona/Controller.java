package Krona;

import Metaphlan.ArgumentBuilder;
import Metaphlan.ArgumentError;
import Metaphlan.CliArgument;
import Metaphlan.FlanRunner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controls the program flow
 * @author dvandeveerdonk
 */
public class Controller {

    /**
     * Used for running metaphlan
     * takes an agumentbuilder and generates metaphlan output
     * @param ab
     */
    public void runMetaphlan(ArgumentBuilder ab){
        CliArgument ca = ab.build();
        FlanRunner fr = new FlanRunner();
        try {
            fr.executeFlan(ca);
        } catch (ArgumentError ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Generates a krona html file using metaphlan output
     * returns the location of his file
     * @param kronaPath
     * @param convPath
     * @param userPath
     * @param mo metaphlan output(s)
     * @return
     */
    public String generateKrona(String kronaPath, String convPath, String userPath ,ArrayList mo){
        try {
            MetaphlanConverter mc = new MetaphlanConverter();
            String moPaths = String.join(",", mo);
            String convOut = userPath+"CONVOUT.xml";
            mc.execute(convPath, moPaths, convOut);
            KronaRunner kr =  new KronaRunner();
            String krOut = userPath+"KRONA.html";
            kr.execute(kronaPath, convOut, krOut);
            return krOut;
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
