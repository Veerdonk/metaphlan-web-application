package Krona;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used for creating a krona html
 * @author dvandeveerdonk
 */
public class KronaRunner implements Execute{
    /**
     * Takes an .xml and transforms it into a krona html page
     * @param kronaLoc
     * @param inFile
     * @param outFile
     * @throws java.io.IOException
     */
    @Override
    public void execute(String kronaLoc, String inFile, String outFile) throws IOException{
        try {
            ProcessBuilder builder = new ProcessBuilder(kronaLoc, inFile, "-o", outFile);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            Logger.getLogger(MetaphlanConverter.class.getName()).log(Level.INFO, "Krona started");
            p.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(KronaRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
