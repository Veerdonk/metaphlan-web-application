<%-- 
    Document   : viewData
    Created on : Jan 20, 2017, 11:26:05 AM
    Author     : dvandeveerdonk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="head.jsp"/>

<h1 id="hideKrona" class="content-subhead pointer">View Krona</h1>
    <div class="pure-form pure-u-2-5 hiddenKrona">
        <form action="${pageContext.request.contextPath}/viewFiles.do" method="POST">      
            <select name="krona" class="pure-u-1-2 dropdown-menu">
                <c:forEach var="krona" items="${sessionScope.krona}">
                    <option value="${krona}">${krona}</option>
                </c:forEach>
            </select>      
            <button class="pure-button pure-u-1-3" type="submit">View Krona</button>
        </form>
    </div>

<h1 id="hideDownload" class="content-subhead pointer">Files you can download</h1>
<div class="pure-form pure-u-2-5 hiddenDownloads">
    <form action="${pageContext.request.contextPath}/viewFiles.do" method="POST">
        <select name="files" class="pure-u-1-2 dropdown-menu">
            <c:forEach var="file" items="${sessionScope.data}">
                <option value="${file}">${file}</option>
            </c:forEach>
        </select>
        <button class="pure-button pure-u-1-3" type="submit">Download</button>
    </form>
</div>
</body>
</html>
