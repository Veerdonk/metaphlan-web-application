<%-- 
    Document   : userWelcome
    Created on : Jan 20, 2017, 11:00:30 AM
    Author     : ahclugtenberg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<h1 class="welcome-user">${sessionScope.userName} (${sessionScope.email}) | <a href="<c:url value="/logout.do" />">LOG OUT</a></h1>
