<%-- 
    Document   : metaphlan
    Created on : 19-jan-2017, 19:37:58
    Author     : ahclugtenbeg
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<form action="submit.do" method="POST" enctype="multipart/form-data">

    <div class="files-content">
        <h1 id="uploadFiles" class="content-subhead pointer">Upload your file(s)</h1>

        <div id="fileUpload">
            <div class="pure-u-2-5 pure-form pure-g file-upload">

                <fieldset class="pure-group">
                    <label for="file-upload" class="custom-file-upload">
                        <!--<i class="fa fa-cloud-upload" class="pure-input-1-2"></i> Upload your files...-->
                        <div class="pure-button pure-u-1" href="#">Select file(s) to upload...</div>
                    </label>
                </fieldset>

                <fieldset class="pure-group">
                    <label for="inputFileType" class="pure-u-1-3">Format input file</label>
                    <select name="inputFileType" id="inputFileType" class="pure-u-1-2 dropdown-menu">
                        <option selected="selected" value="fastq">fastq</option>
                        <option value="fasta">fasta</option>
                        <option value="multifasta">multifasta</option>
                        <option value="bt2">bowtie2out</option>
                        <option value="SAM">SAM</option>
                    </select>
                </fieldset>

                <fieldset class="pure-group">
                    <div id="myDropdown">
                        <label for="myDropdown" class="pure-u-1-3">Multifasta</label>
                        <select name="sensitivity" id="sensitivity" class="pure-u-1-2 dropdown-menu">
                            <option value="sensitive">sensitive</option>
                            <option selected="selected" value="very-sensitive">very-sensitive</option>
                            <option value="sensitive-local">sensitive-local</option>
                            <option value="very-sensitive-local">very-sensitive-local</option>         
                        </select>
                    </div>
                </fieldset>             
            </div>

            <div class="selected-files pure-u-2-5">
                Selected files:
                <div id="file-selected"></div>
                <input type="file" name="file" multiple="true" id="file-upload">
            </div>

            <div class="pure-g">

                <div id="warningMessage" class="pure-u-2-5">The file(s) is/are not the right format</div>
                <div class="pure-u-2-5"></div>
                <button id="submit" class="pure-u-1-5 pure-button">Submit file(s)</button>
            </div>

            <h1 class="content-subhead pointer advanced">
                Advanced Options
            </h1>

            <div id="advancedOptions" class="pure-g pure-form pure-form-stacked">

                <fieldset class="pure-u-1 pure-u-md-1-5">
                    <label for="taxonomicLevel">Taxonomic level</label>
                    <select name="taxonomicLevel">
                        <option value="a">all taxonomic levels</option>
                        <option value="k">kingdoms only</option>
                        <option value="p">phyla only</option>
                        <option value="c">classes only</option>
                        <option value="o">orders only</option>
                        <option value="f">families only</option>
                        <option value="g">genera only</option>
                        <option value="s">species only</option>
                    </select>
                </fieldset>

                <fieldset class="pure-u-1 pure-u-md-1-5">
                    <label for="minCuLen">Minimum total nucleotide length</label>
                    <input placeholder="2000" name="minCuLen" type="number" min="1" style="overflow: hidden;">
                </fieldset>

                <fieldset class="pure-u-1 pure-u-md-1-5">
                    <label for="cores">Cores to use</label>
                    <input placeholder="1" name="cores" type="number" min="1">
                </fieldset>

                <fieldset class="pure-u-1 pure-u-md-2-5">
                    <input type="checkbox" name="ignoreOrganisms" value="Viruses">Do not profile viral organisms<br>
                    <input type="checkbox" name="ignoreOrganisms" value="Eukaryotes">Do not profile eukaryotic organisms<br>
                    <input type="checkbox" name="ignoreOrganisms" value="Bacteria">Do not profile bacterial organisms<br>
                    <input type="checkbox" name="ignoreOrganisms" value="Archaea">Do not profile archeal organisms 
                </fieldset>
            </div>
        </div>
    </div>
</form>
