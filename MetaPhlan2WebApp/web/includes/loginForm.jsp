<%-- 
    Document   : loginForm
    Created on : Jan 16, 2017, 3:00:32 PM
    Author     : dvandeveerdonk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="login-page">
    <div class="error-message">
        <c:choose>
            <c:when test="${requestScope['javax.servlet.forward.request_uri'] != null}">
                ${requestScope.errorMessage}
            </c:when>
        </c:choose>
    </div>
    
    <div class="form pure-form">
        <form action="${pageContext.request.contextPath}/register.do" method="POST" class="register-form">
            <input pattern=".{6,}"  required title="Username should be at least 6 characters" type="text" 
                   placeholder="Username" name="userName">
                   <input type="email" placeholder="E-mail" name="email">
            <input type="password" pattern=".{6,}" required title="Password should be at least 6 characters" 
                   placeholder="Password" name="password" >
            <button type="submit">Create</button>
            <p class="message">Already registered? <a href="#">Sign in</a></p>
        </form>

        <form action="${pageContext.request.contextPath}/login.do" method="POST" class="login-form" >
            <input type="text" placeholder="Username" name="userName">
            <input type="password" placeholder="Password" name="password">
            <button type="submit">Sign in</button>
            <p class="message">Not registered? <a href="#">Create an account</a></p>
        </form>

    </div>
</div> 


