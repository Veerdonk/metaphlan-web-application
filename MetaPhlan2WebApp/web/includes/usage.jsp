<%-- 
    Document   : usage
    Created on : Feb 1, 2017, 3:50:42 PM
    Author     : dvandeveerdonk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<jsp:include page="userWelcome.jsp"/>
<h1 class="content-subhead">
    Basic Usage
</h1>
<div class="pure-u-2-3">
    <p>
        To start a new Metaphlan run select Upload your files. You can then upload 
        your desired files. For basic use you can start the run by clicking submit. 
        Experienced users can click Advanced Options to optimize their run by 
        customizing these options.
        
        For more information and a detailed set-up guide please visit our 
        repository and take a look at the readme. The repository can be found
        <a href="https://bitbucket.org/Veerdonk/metaphlan-web-application">here</a>.
    </p>

    <c:choose>
        <c:when test="${sessionScope.data != null}">
            <p>
                To view your generated Krona diagrams click on View Krona and
                select the file you want to view. You will then be taken to a 
                page where you can view the interactive Krona diagram.
                
                To download the Krona html page or any of the files used to get 
                from Metaphlan input to krona output click the downloads button
                Here you can select the file you want from a list and download it.
            </p>
        </c:when>
    </c:choose>
</div>