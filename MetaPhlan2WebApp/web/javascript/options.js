/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function () {
//    Switches between login and register form on inlog screen
    $('.message a').click(function () {
        $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    });

//    If file-extension is multifasta, sensitivity is shown on screen
    $('#myDropdown').css("visibility", "hidden");
    $("#inputFileType").on("change", function () {
        if (document.getElementById("inputFileType").value === "multifasta") {
            $('#myDropdown').css("visibility", "visible");
        } else {
            $('#myDropdown').css("visibility", "hidden");
        }
    });


    $("#submit").attr('disabled', 'disabled');
//  Checks if file-ext of files is the same as file-ext given by user
    $("#file-upload, #inputFileType").on("change", function () {
        $("#file-selected").html("");
        var e = document.getElementById('inputFileType');
        var strUser = e.value;
        var selection = document.getElementById('file-upload');
        var warningMessage = "";
        console.log("sel" + selection.value);
        console.log("e" + strUser);

        if (selection.value !== "") {
            for (var i = 0; i < selection.files.length; i++) {
//            Adds file-names to div, so they can be shown 
                $("#file-selected").append(selection.files[i].name + "<br/> ");

                var ext = selection.files[i].name.split('.').pop().toLowerCase();
                if (strUser === "bt2") {
                    if (ext !== "txt") {
                        // Bowtie2 files are .txt files
                        warningMessage += "true";
                    }
                } else if (strUser === "multifasta") {
                    if (ext !== "fasta") {
                        // Multifasta files are .fasta files
                        warningMessage += "true";
                    }
                } else {
                    if (ext !== strUser) {
                        warningMessage += "true";
                    }
                }
            }
        } else {
            warningMessage += "true";
        }

        if (warningMessage) {
            // Submit button blocked, because input format is not the same as file extension
            // and a warning is given to the user
            $('#warningMessage').css("visibility", "visible");
            $("#submit").attr('disabled', 'disabled');
        } else {
            $('#warningMessage').css("visibility", "hidden");
            $("#submit").removeAttr('disabled');
        }
    });
    
//    Toggles between showing/not showing krona field
    $("#hideKrona").click(function () {
        if ($('.hiddenKrona').is(':hidden')) {
            $('.hiddenKrona').slideDown();
        } else {
            $('.hiddenKrona').slideUp();
        }
    });
    
//    Toggles between showing/not showing download field
    $("#hideDownload").click(function () {
        if ($('.hiddenDownloads').is(':hidden')) {
            $('.hiddenDownloads').slideDown();
        } else {
            $('.hiddenDownloads').slideUp();
        }
    });
    
//  Toggles between showing/not showing advanced options  
    $(document).ready(function () {
        $('#advancedOptions').hide();
        $('.advanced').click(function () {
            if ($('#advancedOptions').is(':hidden')) {
                $('#advancedOptions').slideDown();
            } else {
                $('#advancedOptions').slideUp();
            }
        });
    });
    
//    Toggles between showing/not showing file-upload field
    $(document).ready(function () {
        $('#fileUpload').hide();
        $('#uploadFiles').click(function () {
            if ($('#fileUpload').is(':hidden')) {
                $('#fileUpload').slideDown();
            } else {
                $('#fileUpload').slideUp();
            }
        });
    });

};

