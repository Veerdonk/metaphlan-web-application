<%-- 
    Document   : home
    Created on : Dec 15, 2016, 1:34:06 PM
    Author     : ahclugtenberg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <jsp:include page="includes/head.jsp" />
    <body>
        <div id="layout" class="pure-g">
            <div class="sidebar pure-u-1 pure-u-md-1-4">
                <div class="header">
                    <h1 class="brand-title">Metaphlan 2</h1>
                    <h2 class="brand-tagline">A simple interface for running the Metaphlan 2 software and visualizing it with Krona</h2>
                    <nav class="nav">
                        <ul class="nav-list">
                            <li class="nav-item">
                                <a class="pure-button" href="https://bitbucket.org/biobakery/biobakery/wiki/metaphlan2">Metaphlan 2</a>
                            </li>
                            <li class="nav-item">
                                <a class="pure-button" href="https://github.com/marbl/Krona/wiki">Krona</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="content pure-u-1 pure-u-md-3-4">
                <c:choose>
                    <c:when test="${sessionScope.userName != null}">
                        <jsp:include page="includes/usage.jsp"/>
                        <jsp:include page="includes/metaphlan.jsp" />
                        <c:choose>
                            <c:when test="${sessionScope.data != null}">
                                <jsp:include page="includes/viewData.jsp"/>
                            </c:when>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <jsp:include page="includes/loginForm.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
