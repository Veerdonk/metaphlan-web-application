# Read me

This readme contains info to get started with the Metaphlan2 web application.

## How do I get set up?
First of all clone this repository to your own computer and open it with your IDE.

###Libraries 
**Important: this application only runs with Java 8**

In order for this application to function, some libraries were used. Some of these have to be downloaded and added to the libraries folder in the project. 

* jQuery 1.11.3
* JDK 8.1
* Apache Tomcat or TomEE 8.0.28
* MysqlConnector 5.1.27 - download from: https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.27
* taglibs-standard-impl-1.2.5 - download from: https://tomcat.apache.org/download-taglibs.cgi
* taglibs-standard-spec-1.2.5 - download from: https://tomcat.apache.org/download-taglibs.cgi
* Jstl 1.2 - download from: https://mvnrepository.com/artifact/javax.servlet/jstl/1.2

### External programs
For this application to be able to run there are some external programs needed. It might be useful to make a new directory on your computer to store these programs.

__Metaphlan 2__ 

Metaphlan 2 can be downloaded from: https://bitbucket.org/biobakery/biobakery/wiki/metaphlan2

To install Metaphlan 2 follow the installation guide: https://bitbucket.org/biobakery/biobakery/wiki/metaphlan2#rst-header-installation

__Conversion script__

The conversion script metaphlan2kronaxml.py is found in the download section of this repo. Just downloading it will fullfill, no further installation needed.

__Krona__

Download KronaTools-2.7 from this page: https://github.com/marbl/Krona/wiki/Downloads
To install KronaTools, unpack the archive, cd to the resulting directory on a command line, and run ```./install.pl.```

* ```--prefix <path>``` - scripts will be installed in the bin directory within this path. The default is /usr/local/.

### Configuration files
There are two files which need to be adjusted to get the program working.

__web.xml__

The web.xml file is found in MetaPhlan2WebApp/Configuration Files.
In this file the locations of the external programs are specified, and these need to be adjusted to match the locations on your computer.

The first one is **metaphlan2**, found beneath
```xml
<description>path to metaphlan2 executable</description>
    <param-name>metaphlanPath</param-name>
```
The path of the <param-value> must be set to the path to your metaphlan2, found in your [TO DO] metaphlan2 installation directory.
```xml
<param-value>/path/to/metaphlan2.py</param-value>
```

The second one is **conversion script**, found beneath
```xml
<description>path to conversion script</description>
        <param-name>converterPath</param-name>
```
The param-value should be adjusted to match the path to the conversion script which was downloaded from the download section of this repository.
```xml
<param-value>/path/to/metaphlan2kronaxml.py</param-value>
```

The third one is **krona**, found beneath
```xml
<description>path to kronaxml executable</description>
        <param-name>kronaPath</param-name>
```
The file needed here is ktImportXML found in the bin directory of where you installed KronaTools.
```xml
<param-value>/path/to/ktImportXML</param-value>
```

The fourth one is a directory found in this project directory. It's a directory where the uploaded files of the user are stored. This path needs to be specified to where this project is stored on your computer. This one is found beneath
```xml
<description>path to data storage</description>
        <param-name>dataPath</param-name>
```

```xml
<param-value>/path/to/YOUR_PROJECTS_FOLDER/thema10/MetaPhlan2WebApp/build/web/userData/
```
__DBProps.properties__

The DBProps.properties file is found in MetaPhlan2WebApp/Source Packages/dao/. This file contains settings for the database used to store user information. This needs to be adjusted to contain your database account information

```xml
url=jdbc:mysql://YOUR_HOST/YOUR_DATABASE
user=YOUR_USERNAME
pass=YOUR_PASSWORD
```

In order for the web application to function you need to create a new table in your database. There's a .sql file named 'users.sql' found under the download section to set this up easily.
From your command line run
```$ mysql < users.sql```

##What does this web application do?

After installing the programs and adjusting the files it's time to start using the web application!

First of all, if you don't have your own files, test files can be found on the human biome project website: http://hmpdacc.org/.

To get started, you should create a new account and login to be able to use the service. After this you'll be taken to the screen where you can upload your file(s) to be used by the web application and create a Krona file. This is the basic functionality of the application. After you've uploaded your files, the program will run and this will take some time. Once the program is done, you'll be logged out automatically. If you now log in again there'll be two things you can do. Either view the krona file, or download files to your computer. 

There also are four advanced options which are implemented and can be accessed by clicking on advanced options:

* Which taxonomic level
* Minimum nucleotide length
* Cores to use
* Which organisms to profile

##Contact
Any questions about the program or it's functioning can be directed at David van de Veerdonk email: d.van.de.veerdonk@st.hanze.nl or Anouk Lugtenberg email: a.h.c.lugtenberg@st.hanze.nl